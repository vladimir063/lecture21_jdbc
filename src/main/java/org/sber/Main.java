package org.sber;

import org.sber.model.Category;
import org.sber.model.DAL.ProductRepository;
import org.sber.model.DAL.Repository;
import org.sber.model.Product;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        try {
            connection = ConnectionManager.open();
            connection.setAutoCommit(false);
            Repository repository = new ProductRepository(connection);
            // INSERT
            Product product1 = new Product();
            product1.setName("potato");
            product1.setCategory(Category.FOOD);
            product1.setManufacturer("KFC");

            String str1 = "2022-04-08 12:30";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime localDateTime1 = LocalDateTime.parse(str1, formatter);
            product1.setManufactureDate(localDateTime1);

            product1.setHasExpiryTime(true);
            repository.save(product1);

            // INSERT
            Product product2 = new Product();
            product2.setName("computer");
            product2.setCategory(Category.TECHNIC);
            product2.setManufacturer("KFC");

            String str = "2021-01-05 11:30";
            LocalDateTime localDateTime2 = LocalDateTime.parse(str, formatter);
            product2.setManufactureDate(localDateTime2);

            product2.setHasExpiryTime(false);
            repository.save(product2);


            // SELECT
            System.out.println(repository.findAll());

            // DELETE
            repository.delete(product2.getId());

            // SELECT
            System.out.println(repository.findAll());

            connection.commit();
        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }
            throw e;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
}
