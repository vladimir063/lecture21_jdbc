package org.sber.model.DAL.exeption;

import java.sql.SQLException;

public class DALException extends RuntimeException {

    public DALException(SQLException throwable) {
        super(throwable);
    }
}
