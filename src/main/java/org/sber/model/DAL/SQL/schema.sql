CREATE TABLE product (
                       id bigserial,
                       name varchar(255) NOT NULL UNIQUE,
                       category varchar(255) NOT NULL,
                       manufacture_date timestamp NOT NULL ,
                       manufacturer varchar(255) NOT NULL,
                       has_expiry_time boolean
);

