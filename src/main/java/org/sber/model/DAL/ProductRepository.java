package org.sber.model.DAL;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.sber.model.Category;
import org.sber.model.DAL.exeption.DALException;
import org.sber.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepository implements Repository {

    private final Connection connection;
    private static final String FIND_ALL_SQL = """
            SELECT id,
                name,
                category,
                manufacture_date,
                manufacturer,
                has_expiry_time
            FROM product
            """;
    private static final String DELETE_SQL = """
            DELETE FROM product
            WHERE id = ?
            """;
    private static final String SAVE_SQL = """
            INSERT INTO product (name, category, manufacture_date, manufacturer, has_expiry_time) 
            VALUES (?, ?, ?, ?, ?);
            """;
    private static final String UPDATE_SQL = """
            UPDATE product
            SET name = ?,
                category = ?,
                manufacture_date = ?,
                manufacturer = ?,
                has_expiry_time = ?
            WHERE id = ?
            """;

    public ProductRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Product save(Product product) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_SQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getCategory().toString());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(product.getManufactureDate()));
            preparedStatement.setString(4, product.getManufacturer());
            preparedStatement.setBoolean(5, product.isHasExpiryTime());

            preparedStatement.executeUpdate();

            var generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                product.setId(generatedKeys.getLong("id"));
            }
            return product;
        } catch (SQLException throwables) {
            throw new DALException(throwables);
        }
    }

    private Product buildProduct(ResultSet resultSet) throws SQLException {
        return new Product(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                Category.valueOf(resultSet.getString("category")),
                resultSet.getTimestamp("manufacture_date").toLocalDateTime(),
                resultSet.getString("manufacturer"),
                resultSet.getBoolean("has_expiry_time")
        );
    }

    @Override
    public List<Product> findAll() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_SQL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Product> products = new ArrayList<>();
            while (resultSet.next()) {
                products.add(buildProduct(resultSet));
            }
            return products;
        } catch (SQLException throwable) {
            throw new DALException(throwable);
        }
    }

    @Override
    public boolean delete(Long id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL)) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException throwable) {
            throw new DALException(throwable);
        }
    }

    @Override
    public void update(Product product) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getCategory().toString());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(product.getManufactureDate()));
            preparedStatement.setString(4, product.getManufacturer());
            preparedStatement.setBoolean(5, product.isHasExpiryTime());
            preparedStatement.setLong(6, product.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throw new DALException(throwables);
        }
    }
}
