package org.sber.model.DAL;

import org.sber.model.Product;

import java.util.List;

public interface Repository {

    Product save(Product product);

    List<Product> findAll();

    boolean delete(Long id);

    void update(Product product);

}
