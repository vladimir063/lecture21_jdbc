package org.sber.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private Long id;

    private String name;

    private Category category;

    private LocalDateTime manufactureDate;

    private String manufacturer;

    private boolean hasExpiryTime;

}
