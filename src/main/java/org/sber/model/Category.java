package org.sber.model;

public enum Category {
    COSMETICS, FOOD, CHEMICAL, TECHNIC
}
